package com.example.nrezo.bracathon;

public class Model_Nijeke_Jano_Listview_Item {
    private int mImageDrawable;

    public Model_Nijeke_Jano_Listview_Item() {

    }
    public Model_Nijeke_Jano_Listview_Item(int mImageDrawable) {
        this.mImageDrawable = mImageDrawable;
    }
    public int getmImageDrawable() {
        return mImageDrawable;
    }
    public void setmImageDrawable(int mImageDrawable) {
        this.mImageDrawable = mImageDrawable;
    }
}
