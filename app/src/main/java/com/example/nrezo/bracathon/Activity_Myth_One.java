package com.example.nrezo.bracathon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class Activity_Myth_One extends AppCompatActivity {
    int myths_number;
    //declaration of the common portion of ids
    String imageId="mythsimage";
    String titleId="mythstitle";
    String bodyTextId="mythsblastertext";
    ImageView imageView;
    boolean flag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__myth__one);
        Bundle extras = getIntent().getExtras();
        flag=false;

        if(extras !=null)
        {
            myths_number = extras.getInt("id");

            //getting the reference from the xml and setting the appropriate value which pressed in the previous screen
            imageId+=myths_number;
            imageView = findViewById(R.id.image_myth);
            int imageResourceId=getImageIdentifier(getBaseContext(),imageId);
            imageView.setImageResource(imageResourceId);
            //setting the title after getting the value from the dialer in the mother acitivity
            TextView texttitle= findViewById(R.id.text_text_title);
            titleId+=myths_number;
            int id=getStringIdentifier(getBaseContext(),titleId);
            String tId=getString(id);
            Spanned result = Html.fromHtml(tId);
            texttitle.setText(result);
            Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/SolaimanLipi_20-04-07.ttf");
            texttitle.setTypeface(custom_font);

        }
        final TextView textOne= findViewById(R.id.text_myth_click);
        textOne.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(flag==false) {
                    flag=true;
                    bodyTextId += myths_number;
                    int id = getStringIdentifier(getBaseContext(), bodyTextId);
                    String rId = getString(id);
                    textOne.setText(rId);
                    Spanned result = Html.fromHtml(rId);
                    textOne.setText(result);
                    Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/SolaimanLipi_20-04-07.ttf");
                    textOne.setTypeface(custom_font);
                    textOne.setTextSize(18);
                    textOne.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.slide_right));
                }
            }
        });
    }

    public static int getStringIdentifier(Context context, String name) {
        return context.getResources().getIdentifier(name, "string", context.getPackageName());
    }
    public static int getImageIdentifier(Context context, String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }










    //follwoing portion is written for the menu
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu:
                startActivity(new Intent(this, MainMenuActivity.class));
                return true;
            case R.id.nijeke_jano:
                startActivity(new Intent(this, Activity_Nijeke_Jano_ListView.class));
                return true;
            case R.id.abeg_onuvuti:
                startActivity(new Intent(this, Activity_Abeg_Onuvuti_Bottom_Tile.class));
                return true;
            case R.id.vul_dharona:
                startActivity(new Intent(this, Activity_Myths.class));
                return true;
            case R.id.video:
                startActivity(new Intent(this, Video_activity.class));
                return true;
            case R.id.games:
                //redirection for games goes here like other activity
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
