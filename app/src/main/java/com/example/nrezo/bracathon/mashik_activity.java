package com.example.nrezo.bracathon;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class mashik_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mashik_activity);
        TextView textView= (TextView) findViewById(R.id.mashiktext1);
        String formattedText = getString(R.string.mashik_text_1);
        Spanned result = Html.fromHtml(formattedText);
        textView.setText(result);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/SolaimanLipi_20-04-07.ttf");
        textView.setTypeface(custom_font);
        textView= (TextView) findViewById(R.id.mashiktext2);
        formattedText = getString(R.string.mashik_text_2);
        result = Html.fromHtml(formattedText);
        textView.setText(result);
        textView.setTypeface(custom_font);
        textView= (TextView) findViewById(R.id.mashiktext3);
        formattedText = getString(R.string.mashik_text_3);
        result = Html.fromHtml(formattedText);
        textView.setText(result);
        textView.setTypeface(custom_font);
        textView= (TextView) findViewById(R.id.mashiktext4);
        formattedText = getString(R.string.mashik_text_4);
        result = Html.fromHtml(formattedText);
        textView.setText(result);
        textView.setTypeface(custom_font);
    }











    //follwoing portion is written for the menu
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu:
                startActivity(new Intent(this, MainMenuActivity.class));
                return true;
            case R.id.nijeke_jano:
                startActivity(new Intent(this, Activity_Nijeke_Jano_ListView.class));
                return true;
            case R.id.abeg_onuvuti:
                startActivity(new Intent(this, Activity_Abeg_Onuvuti_Bottom_Tile.class));
                return true;
            case R.id.vul_dharona:
                startActivity(new Intent(this, Activity_Myths.class));
                return true;
            case R.id.video:
                startActivity(new Intent(this, Video_activity.class));
                return true;
            case R.id.games:
                //redirection for games goes here like other activity
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
