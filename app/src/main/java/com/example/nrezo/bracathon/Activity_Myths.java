package com.example.nrezo.bracathon;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Activity_Myths extends AppCompatActivity implements View.OnClickListener{
    Handler handler;
    ImageView imgPointer, imgClock;
    ValueAnimator clockAnimator;
    ImageView one,two,three,four,five,six,seven,eight,nine,ten,eleven;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__myths);

        imgPointer = (ImageView) findViewById(R.id.imgPointer);
        imgClock = (ImageView) findViewById(R.id.imgClock);
        imgClock.setOnClickListener(this);


/*        imgClock.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),getRand());
                clockAnimator.start();

            }
        });*/
        one = findViewById(R.id.one);
        one.setOnClickListener(this);
        two = findViewById(R.id.two);
        two.setOnClickListener(this);
        three = findViewById(R.id.three);
        three.setOnClickListener(this);
        four = findViewById(R.id.four);
        four.setOnClickListener(this);
        five = findViewById(R.id.five);
        five.setOnClickListener(this);
        six = findViewById(R.id.six);
        six.setOnClickListener(this);
        seven = findViewById(R.id.seven);
        seven.setOnClickListener(this);
        eight = findViewById(R.id.eight);
        eight.setOnClickListener(this);
        nine = findViewById(R.id.nine);
        nine.setOnClickListener(this);
        ten = findViewById(R.id.ten);
        ten.setOnClickListener(this);
        eleven = findViewById(R.id.eleven);
        eleven.setOnClickListener(this);

    }


    private ValueAnimator animatePointer(long orbitDuration, int x) {

        ValueAnimator anim = ValueAnimator.ofInt(0, x);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) imgPointer.getLayoutParams();
                layoutParams.circleAngle = val;
                imgPointer.setLayoutParams(layoutParams);
            }
        });
        anim.setDuration(orbitDuration);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatMode(ValueAnimator.RESTART);
        anim.setRepeatCount(0);
        return anim;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onResume() {
        super.onResume();
        if (clockAnimator != null) {
            if (clockAnimator.isPaused()) {
                clockAnimator.resume();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onPause() {
        super.onPause();
        if ( clockAnimator !=null && clockAnimator.isRunning()) {
            clockAnimator.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ( clockAnimator !=null && clockAnimator.isRunning()) {
            clockAnimator.cancel();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.one:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),30);
                clockAnimator.start();
                delayBeforeStart(1);
                break;
            case R.id.two:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),60);
                clockAnimator.start();
                delayBeforeStart(2);
                break;
            case R.id.three:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),90);
                clockAnimator.start();
                delayBeforeStart(3);
                break;
            case R.id.four:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),120);
                clockAnimator.start();
                delayBeforeStart(4);
                break;
            case R.id.five:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),150);
                clockAnimator.start();
                delayBeforeStart(5);
                break;
            case R.id.six:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),180);
                clockAnimator.start();
                delayBeforeStart(6);
                break;
            case R.id.seven:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),210);
                clockAnimator.start();
                delayBeforeStart(7);
                break;
            case R.id.eight:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),240);
                clockAnimator.start();
                delayBeforeStart(8);
                break;
            case R.id.nine:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),270);
                clockAnimator.start();
                delayBeforeStart(9);
                break;
            case R.id.ten:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),300);
                clockAnimator.start();
                delayBeforeStart(10);
                break;
            case R.id.eleven:
                // do your code

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),330);
                clockAnimator.start();
                delayBeforeStart(11);
                break;
            case R.id.imgClock:
                //getting the random value for the middle button click
                int val=getRand();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),val);
                clockAnimator.start();
                val=val/30;
                delayBeforeStart(val);
                break;
            default:
                break;
        }

    }

    protected int getRand(){
        Random rand = new Random();

        return rand.nextInt(12)*30;
    }

    protected void delayBeforeStart(final int intentNo){
        handler = new Handler();
        handler.postDelayed(new Runnable(){
            @Override
            public void run(){
                if(intentNo!=0 && intentNo!=11) {//if it is 0 then nothing goes anywhere
                    Intent i = new Intent(getBaseContext(), Activity_Myth_One.class);
                    i.putExtra("id", intentNo);
                    startActivity(i);
                }else if(intentNo==11){
                    Intent purushIntent = new Intent(getBaseContext(),Activity_purush_puran.class);
                    startActivity(purushIntent);
                }
            }
        }, 1000);
    }









    //follwoing portion is written for the menu
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu:
                startActivity(new Intent(this, MainMenuActivity.class));
                return true;
            case R.id.nijeke_jano:
                startActivity(new Intent(this, Activity_Nijeke_Jano_ListView.class));
                return true;
            case R.id.abeg_onuvuti:
                startActivity(new Intent(this, Activity_Abeg_Onuvuti_Bottom_Tile.class));
                return true;
            case R.id.vul_dharona:
                startActivity(new Intent(this, Activity_Myths.class));
                return true;
            case R.id.video:
                startActivity(new Intent(this, Video_activity.class));
                return true;
            case R.id.games:
                //redirection for games goes here like other activity
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
