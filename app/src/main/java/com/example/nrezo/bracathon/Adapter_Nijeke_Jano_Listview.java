package com.example.nrezo.bracathon;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class Adapter_Nijeke_Jano_Listview extends ArrayAdapter<Model_Nijeke_Jano_Listview_Item> {
    private Context mContext;
    private List<Model_Nijeke_Jano_Listview_Item> image_resource_list = new ArrayList<>();

    public Adapter_Nijeke_Jano_Listview(@NonNull Context context, ArrayList<Model_Nijeke_Jano_Listview_Item> image_resource_list) {
        super(context,0, image_resource_list);
        mContext = context;
        this.image_resource_list=image_resource_list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.nijeke_jano_listview_item,parent,false);
        Model_Nijeke_Jano_Listview_Item currentItem = image_resource_list.get(position);
        ImageView image = (ImageView)listItem.findViewById(R.id.imageView);
        image.setImageResource(currentItem.getmImageDrawable());
        return listItem;
    }
}
