package com.example.nrezo.bracathon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import java.util.ArrayList;

public class Activity_Nijeke_Jano_ListView extends AppCompatActivity {
    private ListView listView;
    private Adapter_Nijeke_Jano_Listview adapter_nijeke_jano_listview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__nijeke__jano__list_view);
        listView = (ListView) findViewById(R.id.item_list_list_view);
        //imagelist will hold the list of instance Model_Nijeke_Jano_Listview_Item
        ArrayList<Model_Nijeke_Jano_Listview_Item> imageList = new ArrayList<>();
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.boyosshondhi));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.bodyimage));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.shopnodosh));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.hostomithun));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.mashik));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.poribarporikolpona));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.jounota));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.jounorog));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.jounonipirito));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.gender));
        imageList.add(new Model_Nijeke_Jano_Listview_Item(R.drawable.sommotivideo));
        adapter_nijeke_jano_listview = new Adapter_Nijeke_Jano_Listview(this,imageList);
        listView.setAdapter(adapter_nijeke_jano_listview);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent;

                switch(i){
                    case 0:
                         intent=new Intent(getApplicationContext(),ActivityBoyoshShondhiKal.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent=new Intent(getApplicationContext(),sharirik_proticchobi_activity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        intent=new Intent(getApplicationContext(),Activity_shwapno_dosh.class);
                        startActivity(intent);
                        break;
                    case 3:
                        intent=new Intent(getApplicationContext(),Activity_hastho_mithun.class);
                        startActivity(intent);
                        break;
                    case 4:
                        intent=new Intent(getApplicationContext(),mashik_activity.class);
                        startActivity(intent);
                        break;
                    case 5:
                        intent=new Intent(getApplicationContext(),poribar_porikolpona_activity.class);
                        startActivity(intent);
                        break;
                    case 6:
                        intent=new Intent(getApplicationContext(),Activity_jouno.class);
                        startActivity(intent);
                        break;
                    case 7:
                        intent=new Intent(getApplicationContext(),jounorog_activity.class);
                        startActivity(intent);
                        break;
                    case 8:
                        intent=new Intent(getApplicationContext(),nijeke_jano_jouno_nipiron_sohingshota.class);
                        startActivity(intent);
                        break;
                    case 9:
                        intent=new Intent(getApplicationContext(),nijeke_jano_gender_activity.class);
                        startActivity(intent);
                        break;
                }

            }
        });
    }


    //follwoing portion is written for the menu
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu:
                startActivity(new Intent(this, MainMenuActivity.class));
                return true;
            case R.id.nijeke_jano:
                startActivity(new Intent(this, Activity_Nijeke_Jano_ListView.class));
                return true;
            case R.id.abeg_onuvuti:
                startActivity(new Intent(this, Activity_Abeg_Onuvuti_Bottom_Tile.class));
                return true;
            case R.id.vul_dharona:
                startActivity(new Intent(this, Activity_Myths.class));
                return true;
            case R.id.video:
                startActivity(new Intent(this, Video_activity.class));
                return true;
            case R.id.games:
                //redirection for games goes here like other activity
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
