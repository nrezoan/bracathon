package com.example.nrezo.bracathon;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Activity_purush_puran extends AppCompatActivity implements View.OnClickListener {
    Button forward,previous;
    TextView myth,mythBlaster;
    String mythStringId ="purushpurantext";
    String mythBlasterStringId="purushpuranblustertext";
    int counter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purush_puran);
        counter=1;
        forward = findViewById(R.id.btnNext);
        forward.setOnClickListener(this);
        previous=findViewById(R.id.btnPrevious);
        previous.setOnClickListener(this);
        myth=findViewById(R.id.viewMyth);
        mythBlaster=findViewById(R.id.viewBlaster);
        setStringIntoViews(counter);


    }

    protected  void setStringIntoViews(int counter){
        String mythIdString = this.mythStringId+counter;
        String mythBlasterStringId=this.mythBlasterStringId+counter;
        int mythId=getStringIdentifier(getBaseContext(),mythIdString);
        int mythBlasterId=getStringIdentifier(getBaseContext(),mythBlasterStringId);


        settingText(mythId,this.myth);
        settingText(mythBlasterId,this.mythBlaster);


    }


    public static int getStringIdentifier(Context context, String name) {
        return context.getResources().getIdentifier(name, "string", context.getPackageName());
    }

    public void settingText(int id,TextView textOne){
        String rId = getString(id);
        textOne.setText(rId);
        Spanned result = Html.fromHtml(rId);
        textOne.setText(result);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/SolaimanLipi_20-04-07.ttf");
        textOne.setTypeface(custom_font);
        textOne.setTextSize(18);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnNext && this.counter<8){
            this.counter+=1;
            setStringIntoViews(counter);
        }else if(v.getId()==R.id.btnPrevious && this.counter>1){
            this.counter-=1;
            setStringIntoViews(counter);
        }
    }
}
