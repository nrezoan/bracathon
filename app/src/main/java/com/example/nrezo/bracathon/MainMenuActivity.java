package com.example.nrezo.bracathon;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ImageView topLeftTileNijekeJano = (ImageView) findViewById(R.id.topleft);
        topLeftTileNijekeJano.setOnClickListener(this);
        ImageView bottomMiddleTileAbegOnuvuti = (ImageView) findViewById(R.id.bottom_middle_tile_abeg_onuvuti);
        bottomMiddleTileAbegOnuvuti.setOnClickListener(this);
        ImageView middleleftVideoButton = (ImageView) findViewById(R.id.middleleft);
        middleleftVideoButton.setOnClickListener(this);
        ImageView topRightButton = (ImageView) findViewById(R.id.topright);
        topRightButton .setOnClickListener(this);
        ImageView middlerightButton=(ImageView)findViewById(R.id.middleright);
        middlerightButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.topleft:
                intent=new Intent(getApplicationContext(),Activity_Nijeke_Jano_ListView.class);
                startActivity(intent);
                break;
            case R.id.bottom_middle_tile_abeg_onuvuti:
                intent=new Intent(getApplicationContext(),Activity_Abeg_Onuvuti_Bottom_Tile.class);
                startActivity(intent);
                break;
            case R.id.middleleft:
                intent=new Intent(getApplicationContext(),Video_activity.class);
                startActivity(intent);
                break;
            case R.id.topright:
                intent=new Intent(getApplicationContext(),Activity_Myths.class);
                startActivity(intent);
                break;
            case R.id.middleright:
                 intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://drive.google.com/file/d/1cD7s9IVeMiMB9ZViEw0TusXxvnY-4MAQ/view?usp=drivesdk"));
                startActivity(intent);
                break;

        }
    }




    //follwoing portion is written for the menu
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu:
                startActivity(new Intent(this, MainMenuActivity.class));
                return true;
            case R.id.nijeke_jano:
                startActivity(new Intent(this, Activity_Nijeke_Jano_ListView.class));
                return true;
            case R.id.abeg_onuvuti:
                startActivity(new Intent(this, Activity_Abeg_Onuvuti_Bottom_Tile.class));
                return true;
            case R.id.vul_dharona:
                startActivity(new Intent(this, Activity_Myths.class));
                return true;
            case R.id.video:
                startActivity(new Intent(this, Video_activity.class));
                return true;
            case R.id.games:
                //redirection for games goes here like other activity
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://drive.google.com/file/d/1nAlC8u41IEzzQs8US-aAUDixK_y0uFbq/view?fbclid=IwAR0CxtwV9Dlvtoq8rbItu6FZHEOjRrIp5vvS3lMjwLf9mIqgk_3OXgXCGAA"));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
