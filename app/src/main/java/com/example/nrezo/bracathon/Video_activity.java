package com.example.nrezo.bracathon;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class Video_activity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_activity);




        ImageView cyber_harrasment_button= (ImageView) findViewById(R.id.cyber_harrasment_button);
        cyber_harrasment_button.setOnClickListener(this);
        ImageView sommoti_button= (ImageView) findViewById(R.id.sommoti_button);
        sommoti_button.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.cyber_harrasment_button:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=N3LElx_ho2w"));
                startActivity(intent);
                break;
            case R.id.sommoti_button:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=oQbei5JGiT8"));
                startActivity(intent);
                break;
        }

    }


















    //follwoing portion is written for the menu
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu:
                startActivity(new Intent(this, MainMenuActivity.class));
                return true;
            case R.id.nijeke_jano:
                startActivity(new Intent(this, Activity_Nijeke_Jano_ListView.class));
                return true;
            case R.id.abeg_onuvuti:
                startActivity(new Intent(this, Activity_Abeg_Onuvuti_Bottom_Tile.class));
                return true;
            case R.id.vul_dharona:
                startActivity(new Intent(this, Activity_Myths.class));
                return true;
            case R.id.video:
                startActivity(new Intent(this, Video_activity.class));
                return true;
            case R.id.games:
                //redirection for games goes here like other activity
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
